package view;

import javafx.beans.property.ObjectProperty;
import javafx.geometry.Insets;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.CornerRadii;
import javafx.scene.paint.Color;
import shared.GUICoord;
import shared.PieceSquareColor;


public class SquareGui extends BorderPane implements ChessSquareGui  {

    private final ObjectProperty<Color> color;
    private int id;
    private static int compteur = 0;

    public SquareGui(GUICoord gUICoord, PieceSquareColor squareColor) {

        this.id = SquareGui.compteur++;

        // la couleur est définie par les valeurs par défaut de configuration
        color = PieceSquareColor.BLACK.equals(squareColor) ?
                GuiFactory.blackSquareColor : GuiFactory.whiteSquareColor;
        this.setBackground(new Background(new BackgroundFill(color.get(), CornerRadii.EMPTY, Insets.EMPTY)));
    }

    @Override
    public GUICoord getCoord() {
        return null;
    }

    @Override
    public void resetColor(boolean isLight) {

    }
}
