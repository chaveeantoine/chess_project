package view;

import controller.ChessController;
import javafx.beans.property.IntegerProperty;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import shared.GUICoord;
import javafx.scene.control.Label;
import javafx.geometry.Insets;


import java.awt.*;
import java.util.List;

public class View extends GridPane implements ChessView {

    public View(ChessController ChessController) {
        super();

        Pane grid = new GridView();
        BorderPane setupBoard = new BorderPane();
        grid.setMaxSize(500,500);


        double height = GuiFactory.getHeight();

        grid.setPrefSize( height, height);


        setupBoard.setCenter(grid);
        setupBoard.setBottom(CreateGridPaneTop());
        setupBoard.setLeft(CreateGridPaneLeft());
        setupBoard.setTop(CreateGridPaneTop());
        setupBoard.setRight(CreateGridPaneLeft());

        this.add(setupBoard, 0, 1);
    }
    @Override
    public void setPieceToMoveVisible(GUICoord gUICoord, boolean visible) {

    }

    @Override
    public void resetLight(List<GUICoord> gUICoords, boolean isLight) {

    }

    @Override
    public void movePiece(GUICoord initCoord, GUICoord targetCoord) {

    }

    @Override
    public void undoMovePiece(GUICoord pieceToMoveInitCoord) {

    }

    @Override
    public String getPromotionType() {
        return null;
    }

    @Override
    public void promotePiece(GUICoord gUICoord, String promotionType) {

    }

    private GridPane CreateGridPaneTop(){
        int i = 0 ;
        String tabTop[] = {"a","b","c","d","e","f","g","h"};
        GridPane monGridPane = new GridPane();
        while(i < tabTop.length ){
            Label lab = new Label(tabTop[i]);
            lab.setWrapText(true);
            lab.setMaxHeight(GuiFactory.getHeight());
            lab.setPadding(new Insets(5,25,5,30));
            monGridPane.add(lab,i,0);
            i++;
        }
        return monGridPane;

    }
    private GridPane CreateGridPaneLeft(){
        int i = 0 ;
        String tabTop[] = {"1","2","3","4","5","6","7","8"};
        GridPane monGridPane = new GridPane();
        while(i < tabTop.length ){
            Label lab = new Label(tabTop[i]);
           // lab.setMinWidth(GuiFactory.getHeight());
            lab.setPadding(new Insets(18,5,25,5));
            monGridPane.add( lab,0,i);
            i++;
        }
        return monGridPane;
    }
}
